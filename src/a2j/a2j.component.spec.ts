import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { A2jComponent } from './a2j.component';
import { TopMenuComponent } from './top-menu-component/top-menu.component';
import { MatButtonModule, MatGridListModule, MatIconModule, MatMenuModule } from '@angular/material';
import {StoreService} from './services/state-store.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatMenuModule,
        MatGridListModule,
        MatIconModule,
        MatButtonModule
      ],
      declarations: [
        A2jComponent,
        TopMenuComponent
      ],
      providers: [
        StoreService
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(A2jComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
