import { Injectable, OnInit } from '@angular/core';
import { Observable, Observer, Subscription } from 'rxjs';
import { ICallOptions, IConnectionOptions } from 'autobahn';
import * as Autobahn from 'autobahn';
declare var autobahn: any;

// Here we are abstracting the process I found that ensures a context for a NameSpace dependency.
// Until I/we find a better one this should work 100%, it's just ugly and doesn't show we are using autobahn
// as a dependency in the package.json.

@Injectable({
  providedIn: 'root'
})
export class AutobahnNamespaceHelper {
  public url = 'https://unpkg.com/autobahn-browser@^18/autobahn.js';

  getAutobahnContext$(): Observable<any> {
    return Observable.create((observer: any) => {
      const node: HTMLScriptElement = <HTMLScriptElement>document
        .createElement('script');
      node.src = this.url;
      node.onload = function() {
        observer.next({
          autobanh: autobahn
        });
      };
      document.body.appendChild(node);
    });
  }
}

@Injectable({
  providedIn: 'root'
})
export class AutobahnRoutingService {
  // todo ENV conditional
  public autobahnContext$: Observable<any>;
  private auraOptions: IConnectionOptions = {
    url: 'ws://localhost:3131',
    realm: 'aura.a2j.public'
  };

  constructor(private autobahnHelper: AutobahnNamespaceHelper) {
    this.autobahnContext$ = autobahnHelper.getAutobahnContext$();
  }

  public RPCCall(realm: string, procedureName: string, type: string, payload: {[key: string]: any} = {}): Observable<any> {

    let subscriptions: Subscription; // to handle memory leaks
    const sessionOptions: IConnectionOptions = {
      url: this.auraOptions.url,
      realm: realm
    };

    return Observable.create((observer: Observer<any>) => {
      subscriptions = this.autobahnHelper.getAutobahnContext$()
        .subscribe(() => {
          subscriptions
            .add(this.getSession(sessionOptions)
              .subscribe((session: Autobahn.Session) => {
                const transportPackage = prepTransport(type, payload);
                session.call(procedureName, [], transportPackage, {})
                  .then((response) => {
                    // console.log("response", response);
                    observer.next(response);
                    subscriptions.unsubscribe();
                    observer.complete();
                    console.log('hits after complete');
                  })
                  .catch(err => {
                    observer.error(err);
                  });
              }));
        });
    });
  }

  private getSession(opts: Autobahn.IConnectionOptions): Observable<Autobahn.Session> {
    return Observable.create((obs: Observer<Autobahn.Session>) => {
      const connection = new autobahn.Connection(opts);
      connection.onopen = function(session: Autobahn.Session){
        obs.next(session);
      };
      connection.open();
    });
  }
}

export interface AuraTransport {
  Headers: {
    Origin: string;
    Nonce: string;
    Token: string;
  };
  Type: string;
  Payload: {
    [key: string]: any;
  };
  Options: {
    [key: string]: any;
  };
}

function prepTransport(type: string, payload: {[key: string]: any}, options: {[key: string]: any} = {}): AuraTransport {
  const origin = window.location.origin;
  const nonce = Date.now().toString(); //todo change
  const token = !!localStorage.getItem('session_token') ? localStorage.getItem('session_token') : '';
  return {
    Headers: {
      Origin: origin,
      Nonce: nonce,
      Token: token
    },
    Type: type,
    Payload: {
      ...payload
    },
    Options: {
      ...options
    }
  };
}
