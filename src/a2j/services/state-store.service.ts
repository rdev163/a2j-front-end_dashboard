import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export interface Action {
  type: string;
  payload?: any;
}

export interface States {
  [componentKey: string]: State;
}

export interface State {
  reducer: (state: State, action: Action) => State;
  [key: string]: any;
}

@Injectable({
  providedIn: 'root'
})
export class StoreService extends BehaviorSubject<States> {
  constructor() {
    super({});
  }

  public dispatch(action: Action, componentKey: string): void {
    const next = this.value[componentKey]
      .reducer(this.value[componentKey], action);
    this.next({
      ...this.value,
        [componentKey]: next
      });
  }

  public register(states: States) {
    const registeredState = {
      ...this.value,
      ...states
    };
    this.next(registeredState);
    return this.asObservable();
  }
}
