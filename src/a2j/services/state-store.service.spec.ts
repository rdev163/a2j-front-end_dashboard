import { StoreService } from './state-store.service';
import { inject } from '@angular/core/testing';

describe(
  'StoreService',
  () => {
    const fixtureState = {
      reducer: (state, action) => {
        const {type, payload} = action;
        switch (type) {
          case'testing':
            return {
              ...state,
              ...payload
            };
          default:
            return state;
        }
      },
      foo: 'Bar'
    };
    const registrationTestState = {
      'componentKey': fixtureState
    };
    it(
      'Should initialize with an empty object as a value.',
      () => {
        const service = new StoreService();
        expect(service.value).toEqual({});
    });
    it(
      'should register and return an observable that on subscribe responds with registered state.',
      async() => {
        const service = new StoreService();
        inject([StoreService], (storeService) => {
          storeService.register(registrationTestState)
            .subscribe(state => {
              expect(state).toEqual(registrationTestState);
            });
        });
    });
    it(
      'should dispatch with action and update registered state value on subscription.',
      async() => {
        const updateValue = {titleTest: 'Baz'};
        inject([StoreService], (storeService) => {
          const componentKey = registrationTestState[Object.keys(registrationTestState)[0]];
          storeService.register(registrationTestState);
          storeService.dispatch(
            {
              type: 'testing',
              payload: updateValue
            },
            componentKey
          );
          storeService.subscribe(state => {
            console.log('Test state', state);
            expect(state[componentKey]).toBe(updateValue);
          });
    });
  });
});
