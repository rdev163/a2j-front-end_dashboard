import { async, TestBed } from '@angular/core/testing';
import { AutobahnNamespaceHelper, AutobahnRoutingService } from './autobahn-routing.service';
import { Observable } from 'rxjs';
import {Connection, IConnectionOptions, Session} from 'autobahn';

const FAKE_WS_URL = 'ws://localhost:3131';
const FAKE_REALM = 'unit.test';
const FAKE_TOPIC = 'FakeTopic';
const FAKE_MESSAGE = 'Nerd Life';
const FAKE_GET_SESSION = function(sessionOptions) {
  return Observable.create(observer => {
    observer.next(new FakeSession(sessionOptions));
  });
};
const FAKE_CONNECT = function (connectionOptions) {
  return Observable.create(obs => {
    const FAKE_CONNECTION = new FakeConnection(connectionOptions);
    obs.next(FAKE_CONNECTION);
  });
};
class FakeTopic {
  constructor(public topic: string){}
  async publish(topic = this.topic, message) {
    return this;
  }
}
class FakeSession {
  constructor(public info: {url: string, realm: string}){}
  public async subscribe(topic) {
   return new FakeTopic(topic);
  }
}
class FakeConnection extends Connection {
  constructor(options: IConnectionOptions){
    super(options);
  }
}
describe(
  'AutobahnRoutingService',
  () => {
    beforeEach(async () => {
      return TestBed.configureTestingModule({
        providers: [
          AutobahnRoutingService,
          AutobahnNamespaceHelper
        ]
      });
    });
    it(
      'should be created',
      async () => {
        const service = TestBed.get(AutobahnRoutingService);
        expect(service)
          .toBeTruthy();
      });
    it(
      'should return a socket connection when calling connect.',
      async(() => {
        const service = TestBed.get(AutobahnRoutingService);
        const spy = spyOn(service, 'connect');
        const FAKE_CONNECTION_OPTIONS = {
          url: FAKE_WS_URL,
          realm: FAKE_REALM
        };
        spy.and.callFake(FAKE_CONNECT);
        service.connect(FAKE_CONNECTION_OPTIONS)
          .subscribe(connection => {
            expect(connection)
              .toEqual(new Connection(FAKE_CONNECTION_OPTIONS));
          });
      }
    ));

  it(
    'should create a connection to the default realm and return a session.',
    async() => {
      const service = TestBed.get(AutobahnRoutingService);
      const sessionOptions = {
        url: FAKE_WS_URL,
        realm: FAKE_REALM
      };
      const spy = spyOn(service, 'getSession');
      spy.and.callFake(function(res) {
        expect(res)
          .not.toBe(null);
        return Observable.create(observer => observer.next(res));
      });
      service.getSession(sessionOptions)
        .subscribe(session => {
          console.log('SESSION', session);
          expect(session)
            .toBe(new FakeSession(sessionOptions));
        });
  });
  it(
    'should subscribe to a topic and return subscription.',
    async() => {
      const service = TestBed.get(AutobahnRoutingService);
      const sessionOptions = {
        url: FAKE_WS_URL,
        realm: FAKE_REALM
      };
      const getSessionSpy = spyOn(service, 'getSession');
      getSessionSpy.and.callFake(FAKE_GET_SESSION);
      service.getSession(sessionOptions)
        .subscribe(session => {
          expect(session)
            .toBe(sessionOptions);
          session.publish(FAKE_TOPIC, [FAKE_MESSAGE])
            .then((publishedSession) => {
              expect(publishedSession)
                .toEqual(new FakeSession({
                  url: FAKE_WS_URL,
                  realm: FAKE_REALM
                }));
            });
        });
  });
  it('should publish a message to the topic', () => {

  });
  it('should recieve a meessage when a subscribed topic is published to', () => {

  });
});
