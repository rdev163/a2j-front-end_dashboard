import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatFormFieldModule, MatGridListModule,
  MatIconModule, MatInputModule, MatMenuModule, MatOptionModule,
  MatRippleModule, MatSelectModule
} from '@angular/material';

import { A2jRoutingModule } from './a2j-routing.module';
import { A2jComponent } from './a2j.component';

import { TopMenuComponent } from './top-menu/top-menu.component';
import { HomeComponent } from './home/home.component';
import { TileComponent } from './home/tile/tile.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './login/registration/registration.component';

@NgModule({
  declarations: [
    A2jComponent,
    TopMenuComponent,
    HomeComponent,
    TileComponent,
    LoginComponent,
    RegistrationComponent
  ],
  imports: [
    A2jRoutingModule,
    CommonModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatRippleModule,
    MatMenuModule,
    MatButtonModule,
    MatGridListModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
  ],
  providers: [
  ],
  bootstrap: [A2jComponent]
})
export class A2jModule { }
