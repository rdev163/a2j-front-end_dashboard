import { Component, OnInit, ViewChild } from '@angular/core';
import { Action, State, States, StoreService } from '../services/state-store.service';
import { Observable } from 'rxjs';
import { MatMenuTrigger } from '@angular/material';
import {AutobahnRoutingService} from '../services/autobahn-routing.service';

export interface TopMenuState extends State {
  viewTitle: string;
}

@Component({
  selector: 'a2j-top-menu-component',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss'],
})

export class TopMenuComponent implements OnInit {
  static componentStoreKey = 'TopMenuComponent';
  public componentStoreKey = TopMenuComponent.componentStoreKey;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  public stateRegistration: States = {
    [TopMenuComponent.componentStoreKey]: {
      reducer: TopMenuComponent.reducer,
      viewTitle: 'Access2Justice'
    }
  };
  public state$: Observable<States>;
  static reducer(state: TopMenuState, action: Action): TopMenuState {
      const {type, payload} = action;
      switch (type) {
        case'updateTitle':
          return state.viewTitle = payload;
        default:
          return state;
      }
  }
  constructor(private store: StoreService, private wamp: AutobahnRoutingService) { }
  ngOnInit() {
    this.state$ = this.store.register(this.stateRegistration);
    // this.wamp.connect({url: 'ws://localhost:3131', realm: 'aura.nexus.info'})
    //   .subscribe(session => {
    //     console.log('Session', session);
    //   });
  }
}
/**
 * Actions for dispatcher.
 */
export const TopMenuActions = {
  udpateTitle(nextTitle: string): Action {
    return {
      type: 'updateTitle',
      payload: nextTitle
    };
  }
};
