import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopMenuComponent } from './top-menu.component';
import { MatButtonModule, MatGridListModule,
  MatIconModule, MatMenuModule
} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {A2jComponent} from '../a2j.component';

describe('TopMenuComponent', () => {
  let component: TopMenuComponent;
  let fixture: ComponentFixture<TopMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatMenuModule,
        MatGridListModule,
        MatIconModule,
        MatButtonModule
      ],
      declarations: [
        A2jComponent,
        TopMenuComponent
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
