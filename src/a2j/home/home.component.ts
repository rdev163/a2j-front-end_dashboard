import {Component} from '@angular/core';

@Component({
  selector: 'a2j-home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  public tiles = [
    {
      tileLabel: 'Login',
      icon: 'lock',
      style: 'gradient gold',
      route: '/login'
    },
    {
      tileLabel: 'Documents',
      icon: 'chrome_reader_mode',
      style: 'gradient green',
      route: '/login'
    },
    {
      tileLabel: 'Interview',
      icon: 'portrait',
      style: 'gradient blue',
      route: '/login'
    },
    {
      tileLabel: 'Profiles',
      icon: 'contacts',
      style: 'gradient violet',
      route: '/login'
    }
  ];
  color = 'rgba(114, 216, 213, 0.3)';
  constructor() {}
}
