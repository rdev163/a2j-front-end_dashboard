import {Component, Input} from '@angular/core';

@Component({
  selector: 'a2j-tile-component',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent {
  @Input()
  public routeLink = '';
  @Input()
  public tileLabel = '';
  @Input()
  public icon = 'perm_identity';
  @Input()
  public tileClass = '';
  constructor() {}
  public ngOnInit() {
    console.log('Tile init');
  }

}
