import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMAIL_RE_PATTERN, SPECIAL_CHARS_RE_PATTERN } from '../../common/regexp';
import { AutobahnRoutingService } from '../../services/autobahn-routing.service';
import { Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'a2j-registration',
  templateUrl: './registration.template.html',
  styleUrls: ['./registration.style.scss'],
})
export class RegistrationComponent {
  RegistrationForm: FormGroup = this._fb.group({
    emailFormControl: ['', [Validators.required, Validators.pattern(EMAIL_RE_PATTERN)]],
    usernameFormControl: ['', [Validators.required, Validators.minLength(2)]],
    passwordFormControl: ['', [Validators.required, Validators.minLength(9), Validators.pattern(SPECIAL_CHARS_RE_PATTERN)]],
    testPasswordFormControl: ['']
  });

  constructor(
    private _fb: FormBuilder,
    private _wamp: AutobahnRoutingService) {

  }

  submitRegistration() {
    const username = this.RegistrationForm.controls.usernameFormControl.value;
    const password = this.RegistrationForm.controls.passwordFormControl.value;
    const email = this.RegistrationForm.controls.emailFormControl.value;
    console.log('hit submit', username, password, email);
    this._wamp.RPCCall(
      'aura.a2j.public',
      'user.registration',
      'Registration',
      {
        username: this.RegistrationForm.controls.usernameFormControl.value,
        password: this.RegistrationForm.controls.passwordFormControl.value,
        email: this.RegistrationForm.controls.emailFormControl.value
      }).subscribe(res => console.log(res));
  }

  validateSamePW(event) {
    Observable.create(obs => {
      obs.next(event.target.value);
    }).pipe(debounceTime(1000)).subscribe(() => {
      const a = event.target.value;
      const b = this.RegistrationForm.controls.passwordFormControl.value;
      if (a === b) {
        console.log('hit');
        this.RegistrationForm.controls.testPasswordFormControl.setErrors(null);
      } else {
        this.RegistrationForm.controls.testPasswordFormControl.setErrors({notMatching: true});
      }
    });
  }
}
