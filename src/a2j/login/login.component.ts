import {Component, OnInit} from '@angular/core';
import {AutobahnRoutingService} from '../services/autobahn-routing.service';

@Component({
  selector: 'a2j-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public options = {
    value: {
      hideRequired: true,
      floatLabel: 'I am a label'
    }
  };
  public formValues = {
    username: '',
    password: ''
  };

  constructor(private aura: AutobahnRoutingService) {
  }

  public onLogin() {
    // const username: string = this.loginForm.username;
    this.aura.RPCCall('ctl.authentication', null, 'Login', {username: this.formValues.username, password: this.formValues.password})
      .subscribe(loginResponse => {
        console.log('Auth responded\n', loginResponse);
        if (loginResponse.kwargs && loginResponse.kwargs.jwt) {
          const jwt = loginResponse.kwargs.jwt;
          sessionStorage.setItem('JWT', jwt);
        }

      })
      .unsubscribe();
  }
}
