export interface Question {
  type: string
  answer: any
}

export const WhatsYourAddress: Question = {
  type: 'single line text',
  answer: '1234 Main St',
};

