[![Waffle.io - Columns and their card count](https://badge.waffle.io/CodeForPortland/a2j-front-end_dashboard.svg?columns=all)](https://waffle.io/CodeForPortland/a2j-front-end_dashboard)
[![Known Vulnerabilities](https://snyk.io/test/github/CodeForPortland/a2j-front-end_dashboard/badge.svg?targetFile=package.json)](https://snyk.io/test/github/CodeForPortland/a2j-front-end_dashboard?targetFile=package.json)
# Access2Justice Dashboard

A dashboard component that allows for navigation between the different front end components for [Access2Justice](https://www.codeforportland.org/access2justicePDX/).

# Modules and Components

Front end components can be integrated most easily using an `NGModule`. To start your own component it is recommended to use the [Angular CLI](https://github.com/angular/angular-cli).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
